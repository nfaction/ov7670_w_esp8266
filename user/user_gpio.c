#include "ets_sys.h"
#include "osapi.h"
#include "os_type.h"
#include <gpio.h>
#include "user_config.h"
#include "user_interface.h"
#include "udp_client.h"
#include "user_gpio.h"

extern int ets_uart_printf(const char *fmt, ...);
uint16 j=0;
uint8 frame_detect=0;
uint16 i,l;
char line[H_RESOL];
int DevBuffer_length;

// Rutina de muestreo de video.
void read_pclk_handler() {
	ETS_GPIO_INTR_DISABLE(); // Disable gpio interrupts

	uint32 gpio_status = GPIO_REG_READ(GPIO_STATUS_ADDRESS);
	//clear interrupt status
	GPIO_REG_WRITE(GPIO_STATUS_W1TC_ADDRESS, gpio_status);

	// Try to detect frame beginning, counting during low-state HREF preamble.
	if (frame_detect == 0) {
		while (GPIO_INPUT_GET(13)==0) {
			l++;
			//ets_uart_printf("waiting ... %d ms\n", l);
			os_delay_us(1000);
			if (l>8) {
				frame_detect = 1;
#if	TESTING
				ets_uart_printf("Frame start detected! Waited %d ms.\n", l);
#endif
				break;
			}
		}
		ETS_GPIO_INTR_ENABLE();
		l=0;
	}
	else if (frame_detect == 1) {
		i=0;
		uint8 rising_edge_detected = 0;
		// Get HREF
		while(GPIO_INPUT_GET(13)==1) {
			if ((GPIO_INPUT_GET(3)==1) && (rising_edge_detected==0)) {
#if TESTING
				line[i] = ( GPIO_INPUT_GET(0) | (GPIO_INPUT_GET(14) << 1) |
						(GPIO_INPUT_GET(15) << 2)  | (GPIO_INPUT_GET(12) << 3) | (GPIO_INPUT_GET(2) << 4) |
						(GPIO_INPUT_GET(4) << 5) | (GPIO_INPUT_GET(5) << 6) | (GPIO_INPUT_GET(5) << 7) );
#else
				line[i] = ( GPIO_INPUT_GET(0) | (GPIO_INPUT_GET(14) << 1) |
						(GPIO_INPUT_GET(15) << 2)  | (GPIO_INPUT_GET(12) << 3) | (GPIO_INPUT_GET(2) << 4) |
						(GPIO_INPUT_GET(4) << 5) | (GPIO_INPUT_GET(5) << 6) | (GPIO_INPUT_GET(1) << 7) );
#endif
				i++;
				rising_edge_detected = 1;
#if DEBUG
				ets_uart_printf("%x ", line[i-1]);
#endif

			}
			else if (GPIO_INPUT_GET(3)==0) {
				rising_edge_detected = 0;
			}
		}
#if TESTING
		ets_uart_printf("%d\n", i);
#endif
		if (i==H_RESOL) {
			if (TESTING>1)	ets_uart_printf("%d %x ", i, line[j+i-1]);
			DevBuffer_length = PACKET_SIZE*H_RESOL;
			ets_uart_printf("Trying to send...\n");
			user_udp_send();
			j++;
		}

		if (j==V_RESOL) {
			j = 0; l = 0;
			frame_detect = 0;
		}
		else
			ETS_GPIO_INTR_ENABLE(); // Enable gpio interrupts
	}
}


void ICACHE_FLASH_ATTR user_gpio_init(void) {
	ets_uart_printf("GPIO and interrupt initialisation...\r\n");
	PIN_FUNC_SELECT(PERIPHS_IO_MUX_GPIO0_U, FUNC_GPIO0);
	gpio_output_set(0, 0, 0, BIT0); // Set GPIO0 as input

#if !TESTING
	PIN_FUNC_SELECT(PERIPHS_IO_MUX_U0TXD_U, FUNC_GPIO1);
	gpio_output_set(0, 0, 0, BIT1); // Set GPIO1 as input
#endif

	PIN_FUNC_SELECT(PERIPHS_IO_MUX_GPIO2_U, FUNC_GPIO2);
	gpio_output_set(0, 0, 0, BIT2); // Set GPIO2 as input
	PIN_FUNC_SELECT(PERIPHS_IO_MUX_U0RXD_U, FUNC_GPIO3);
	gpio_output_set(0, 0, 0, BIT3); // Set GPIO3 as input
	PIN_FUNC_SELECT(PERIPHS_IO_MUX_GPIO4_U, FUNC_GPIO4);
	gpio_output_set(0, 0, 0, BIT4); // Set GPIO4 as input
	PIN_FUNC_SELECT(PERIPHS_IO_MUX_GPIO5_U, FUNC_GPIO5);
	gpio_output_set(0, 0, 0, BIT5); // Set GPIO5 as input
	PIN_FUNC_SELECT(PERIPHS_IO_MUX_MTDI_U, FUNC_GPIO12);
	gpio_output_set(0, 0, 0, BIT1); // Set GPIO13 as input
	PIN_FUNC_SELECT(PERIPHS_IO_MUX_MTMS_U, FUNC_GPIO14);
	gpio_output_set(0, 0, 0, BIT14); // Set GPIO14 as input
	PIN_FUNC_SELECT(PERIPHS_IO_MUX_MTDO_U, FUNC_GPIO15);
	gpio_output_set(0, 0, 0, BIT15); // Set GPIO15 as input

	// HREF on GPIO13 pin
	ETS_GPIO_INTR_DISABLE();
	ETS_GPIO_INTR_ATTACH(read_pclk_handler, BIT(13)); // GPIO interrupt handler
	PIN_FUNC_SELECT(PERIPHS_IO_MUX_MTCK_U, FUNC_GPIO13); // Set GPIO13 function
	gpio_output_set(0, 0, 0, BIT13); // Set GPIO13 as input
	GPIO_REG_WRITE(GPIO_STATUS_W1TC_ADDRESS, BIT(13)); // Clear GPIO13 status
	gpio_pin_intr_state_set(GPIO_ID_PIN(13), GPIO_PIN_INTR_ANYEDGE); // Interrupt on GPIO13  edges
	ETS_GPIO_INTR_ENABLE();
	ets_uart_printf("...init done!\r\n");
}

