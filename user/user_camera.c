/******************************************************************************
 * Copyright 2013-2014 Espressif Systems (Wuxi)
 *
 * FileName: user_humiture.c
 *
 * Description: humiture demo's function realization
 *
 * Modification history:
 *     2014/5/1, v1.0 create this file.
 *******************************************************************************/
#include "ets_sys.h"
#include "osapi.h"
#include "os_type.h"
#include "user_config.h"
#include "user_interface.h"

#include "user_camera.h"
#include "driver/i2c_master.h"


LOCAL uint8 camera_data[200];

/******************************************************************************
 * FunctionName : wrOV7670Reg
 * Description  : config ov7670's internal data
 * Parameters   : uint8 addr - ov7670's address
 *                uint8 regId - register sub-address
 *                uint8 regValue - value to be written
 *                uint16 len - read length
 * Returns      : bool - true or false
 *******************************************************************************/
LOCAL bool ICACHE_FLASH_ATTR
wrOV7670Reg(uint8 regId, uint8 regValue)
{
	uint8 ack;
	uint16 i;

	///////////////////////////////////
	/////// 3-phase WRITE TX CYCLE ///////
	///////////////////////////////////

	/////////////  Write slave base address (Phase 1)
	i2c_master_start();
	i2c_master_writeByte(OV7670_Addr);
	ack = i2c_master_getAck();
	if (ack) {
		ets_uart_printf("addr not ack when tx write cmd \n");
		i2c_master_stop();
		return false;
	}
	////////////////////// Write slave RegID (Phase 2)
	i2c_master_writeByte(regId);
	ack = i2c_master_getAck();
	if (ack) {
		ets_uart_printf("addr not ack when tx write cmd \n");
		i2c_master_stop();
		return false;
	}
	//////////////////// Write actual data (Phase 3)
	i2c_master_writeByte(regValue);  //divide xclk by ...
	ack = i2c_master_getAck();
	if (ack) {
		ets_uart_printf("addr not ack when tx write cmd \n");
		i2c_master_stop();
		return false;
	}
	i2c_master_stop();
	i2c_master_wait(2000);

	// 2-PHASE READ TX CYCLE
	////////////// Read value in RegID address.
	i2c_master_start();
	i2c_master_writeByte(OV7670_Addr+1);
	ack = i2c_master_getAck();
	if (ack) {
		ets_uart_printf("addr not ack when tx write cmd \n");
		i2c_master_stop();
		return false;
	}

	for (i = 0; i < 1; i++) {
		camera_data[i] = i2c_master_readByte();
		ets_uart_printf("0x%02x : 0x%02x", regId, camera_data[i]);
		i2c_master_setAck((i == (1 - 1)) ? 1 : 0);
	}
	ets_uart_printf("\n");
	i2c_master_stop();

	return true;
}

/******************************************************************************
 * FunctionName : rdOV7670Reg
 * Description  : read ov7670's internal data
 * Parameters   : uint8 addr - ov7670's address
 *                uint8 regId - register sub-address
 *                uint8 regData - pointer to data
 * Returns      : bool - true or false
 *******************************************************************************/
LOCAL bool ICACHE_FLASH_ATTR
rdOV7670Reg(uint8 regId, uint8 *regData )
{
	uint8 ack;
	uint16 i;

	///////////////////////////////////
	/////// 2-phase WRITE TX CYCLE ///////
	///////////////////////////////////

	/////////////  Write slave base address (Phase 1)
	i2c_master_start();
	i2c_master_writeByte(OV7670_Addr);
	ack = i2c_master_getAck();
	if (ack) {
		ets_uart_printf("addr not ack when tx write cmd \n");
		i2c_master_stop();
		return false;
	}
	////////////////////// Write slave RegID (Phase 2)
	i2c_master_writeByte(regId);
	ack = i2c_master_getAck();
	if (ack) {
		ets_uart_printf("addr not ack when tx write cmd \n");
		i2c_master_stop();
		return false;
	}
	i2c_master_stop();
	i2c_master_wait(2000);

	// 2-PHASE READ TX CYCLE
	////////////// Read value in RegID address.
	i2c_master_start();
	i2c_master_writeByte(OV7670_Addr+1);
	ack = i2c_master_getAck();
	if (ack) {
		ets_uart_printf("addr not ack when tx write cmd \n");
		i2c_master_stop();
		return false;
	}

//	for (i = 0; i < len; i++) {
		*regData = i2c_master_readByte();
		ets_uart_printf("Register: 0x%02x, Value: 0x%02x\n", regId, *regData);
		i2c_master_setAck((0 == 0) ? 1 : 0);
	//}
	ets_uart_printf("\n");
	i2c_master_stop();

	return true;
}


/*
 * Store a set of start/stop values into the camera.
 */
static int ov7670_set_hw(int hstart, int hstop,
		int vstart, int vstop)
{
	int ret;
	unsigned char v;
/*
 * Horizontal: 11 bits, top 8 live in hstart and hstop.  Bottom 3 of
 * hstart are in href[2:0], bottom 3 of hstop in href[5:3].  There is
 * a mystery "edge offset" value in the top two bits of href.
 */
	ret =  wrOV7670Reg(REG_HSTART, (hstart >> 3) & 0xff);
	ret += wrOV7670Reg(REG_HSTOP, (hstop >> 3) & 0xff);
	ret += rdOV7670Reg(REG_HREF, &v);
	v = (v & 0xc0) | ((hstop & 0x7) << 3) | (hstart & 0x7);
	os_delay_us(10000);
	ret += wrOV7670Reg(REG_HREF, v);
/*
 * Vertical: similar arrangement, but only 10 bits.
 */
	ret += wrOV7670Reg(REG_VSTART, (vstart >> 2) & 0xff);
	ret += wrOV7670Reg(REG_VSTOP, (vstop >> 2) & 0xff);
	ret += rdOV7670Reg(REG_VREF, &v);
	v = (v & 0xf0) | ((vstop & 0x3) << 2) | (vstart & 0x3);
	os_delay_us(10000);
	ret += wrOV7670Reg(REG_VREF, v);
	return ret;
}


//LOCAL void ICACHE_FLASH_ATTR set_OV7670reg(void)
//{
//	wrOV7670Reg(0x8c, 0x00);
//	wrOV7670Reg(0x3a, 0x04);
//	wrOV7670Reg(0x40, 0xd0);  //原来10，通用COM15，设置输出数据范围和输出格式
//	wrOV7670Reg(0x8c, 0x00);
//	 wrOV7670Reg(0x12, 0x0C); //输出格式QVGA,原设置为0x14
//	wrOV7670Reg(0x32, 0x80);
//	wrOV7670Reg(0x17, 0x16);
//	wrOV7670Reg(0x18, 0x04);
//	wrOV7670Reg(0x19, 0x02);
//	wrOV7670Reg(0x1a, 0x7b);//0x7a,  原来0x7b
//	wrOV7670Reg(0x03, 0x06);//0x0a,  原来0x06
//	 wrOV7670Reg(0x0c, 0x0C);//原来0x04	   com3,设置缩放，省电，模式等
//	wrOV7670Reg(0x3e, 0x00);//	原来0x00   com14，dcw和pclk缩放设置，以及分频等
//	wrOV7670Reg(0x70, 0x3a); //原来0x04
//	wrOV7670Reg(0x71, 0x35); //原来0x35
//	wrOV7670Reg(0x72, 0x11); //原来0x11
//	wrOV7670Reg(0x73, 0x00);//原来f0 已改这里改变很重要
//	wrOV7670Reg(0xa2, 0x00); //原来0x02
//	 wrOV7670Reg(0x11, 0x07); //时钟频率，这里改为最大值，原来为0x81  （改变）
//	//wrOV7670Reg(0x15 , 0x31);
//	wrOV7670Reg(0x7a, 0x20); //原来0x20 寄存器7a--89为伽马曲线设置
//	wrOV7670Reg(0x7b, 0x1c); //原来0x1c
//	wrOV7670Reg(0x7c, 0x28); //原来0x28
//	wrOV7670Reg(0x7d, 0x3c); //原来0x3c
//	wrOV7670Reg(0x7e, 0x55); //原来0x55
//	wrOV7670Reg(0x7f, 0x68); //原来0x68
//	wrOV7670Reg(0x80, 0x76); //原来0x76
//	wrOV7670Reg(0x81, 0x80); //原来0x80
//	wrOV7670Reg(0x82, 0x88); //原来0x88
//	wrOV7670Reg(0x83, 0x8f); //原来0x8f
//	wrOV7670Reg(0x84, 0x96); //原来0x96
//	wrOV7670Reg(0x85, 0xa3); //原来0xa3
//	wrOV7670Reg(0x86, 0xaf); //原来0xaf
//	wrOV7670Reg(0x87, 0xc4); //原来0xc4
//	wrOV7670Reg(0x88, 0xd7); //原来0xd7
//	wrOV7670Reg(0x89, 0xe8); //原来0xe8
//
//	wrOV7670Reg(0x32,0xb6);
//
//	wrOV7670Reg(0x13, 0xff); //原来0xe0 com13 AGC,AWB,AEC使能控制	 （改变）
//	wrOV7670Reg(0x00, 0x00);//AGC //原来0x00
//	wrOV7670Reg(0x10, 0x00);//原来0x00  曝光值
//	wrOV7670Reg(0x0d, 0x00);//原来0x00	 COM4
//	wrOV7670Reg(0x14, 0x4e);//原来0x28, limit the max gain	自动增益设置 没变化
//	wrOV7670Reg(0xa5, 0x05); //原来0x05 50Hz bangding step limting
//	wrOV7670Reg(0xab, 0x07); //原来0x07 60Hz bangding step limting
//	wrOV7670Reg(0x24, 0x75); //原来0x75 agc/aec-稳定运行区域上限
//	wrOV7670Reg(0x25, 0x63); //原来0x63 agc/aec-稳定运行区域下限
//	wrOV7670Reg(0x26, 0xA5); //原来0xa5 agc/aec-快速运行区域
//	wrOV7670Reg(0x9f, 0x78); //原来0x78 基于直方图的aec/agc的控制1
//	wrOV7670Reg(0xa0, 0x68); //原来0x68 基于直方图的aec/agc的控制2
////	wrOV7670Reg(0xa1, 0x03);//0x0b,
//	wrOV7670Reg(0xa6, 0xdf);//0xd8, 原来0xdf 基于直方图的aec/agc的控制3
//	wrOV7670Reg(0xa7, 0xdf);//0xd8, 原来0xdf 基于直方图的aec/agc的控制4
//	wrOV7670Reg(0xa8, 0xf0); //原来0xf0  基于直方图的aec/agc的控制5
//	wrOV7670Reg(0xa9, 0x90);  //原来0x90  基于直方图的aec/agc的控制6
//	wrOV7670Reg(0xaa, 0x94);  //原来0x94  基于直方图的aec/agc的控制7
//	//wrOV7670Reg(0x13, 0xe5);  //原来0xe5
//	wrOV7670Reg(0x0e, 0x61);  //原来0x61 COM5
//	wrOV7670Reg(0x0f, 0x43);  //原来0x4b COM6
//	wrOV7670Reg(0x16, 0x02);  //原来0x02 保留
//	wrOV7670Reg(0x1e, 0x37);//0x07, 原来0x37 水平镜像/竖直翻转使能	 设置为01后发生翻转
//	wrOV7670Reg(0x21, 0x02);	//原来0x02  保留
//	wrOV7670Reg(0x22, 0x91);	//原来0x91	保留
//	wrOV7670Reg(0x29, 0x07);	//原来0x07	保留
//	wrOV7670Reg(0x33, 0x0b);	//原来0x0b	href控制，改为初始值80时改变
//	wrOV7670Reg(0x35, 0x0b);	//原来0xe0	保留
//	wrOV7670Reg(0x37, 0x3f);	//原来0x1d	adc控制
//	wrOV7670Reg(0x38, 0x01);	//原来0x71	adc和模拟共模控制
//	wrOV7670Reg(0x39, 0x00);	//原来0x2a	adc偏移控制
//	wrOV7670Reg(0x3c, 0x78);	//原来0x78	 COM12
//	wrOV7670Reg(0x4d, 0x40);	//原来0x40	 保留
//	wrOV7670Reg(0x4e, 0x20);	//原来0x20	 保留
//	wrOV7670Reg(0x69, 0x00);	//原来0x00	  固定增益控制
//	wrOV7670Reg(0x6b, 0x0a);   //PLL 原来0x00	  pll控制	重要设置
//	wrOV7670Reg(0x74, 0x19);	//原来0x19	手动数字增益
//	wrOV7670Reg(0x8d, 0x4f);	//原来0x4f	保留
//	wrOV7670Reg(0x8e, 0x00);	//原来0x00	保留
//	wrOV7670Reg(0x8f, 0x00);	//原来0x00	保留
//	wrOV7670Reg(0x90, 0x00);	//原来0x00	保留
//	wrOV7670Reg(0x91, 0x00);	//原来0x00	保留
//	wrOV7670Reg(0x92, 0x00);   //0x19,//0x66	 原来0x00 空行低8位
//	wrOV7670Reg(0x96, 0x00);	//原来0x00	  保留
//	wrOV7670Reg(0x9a, 0x80);	//原来0x80	  保留
//	wrOV7670Reg(0xb0, 0x84);	//原来0xe0	  保留
//	wrOV7670Reg(0xb1, 0x0c);	//原来0x0c	 ablc设置
//	wrOV7670Reg(0xb2, 0x0e);	//原来0x0e	  保留
//	wrOV7670Reg(0xb3, 0x82);	//原来0x82	  ablc target
//	wrOV7670Reg(0xb8, 0x0a);	//原来0x0a	   保留
//	wrOV7670Reg(0x43, 0x14);	//原来0x14	  43-48为保留
//	wrOV7670Reg(0x44, 0xf0);	//原来0xf0
//	wrOV7670Reg(0x45, 0x34);	//原来0x34
//	wrOV7670Reg(0x46, 0x58);	//原来0x58
//	wrOV7670Reg(0x47, 0x28);	//原来0x28
//	wrOV7670Reg(0x48, 0x3a);	//原来0x3a
//
//	wrOV7670Reg(0x59, 0x88);	//原来0x88	 51-5e保留
//	wrOV7670Reg(0x5a, 0x88);	//原来0x88
//	wrOV7670Reg(0x5b, 0x44);	//原来0x44
//	wrOV7670Reg(0x5c, 0x67);	//原来0x67
//	wrOV7670Reg(0x5d, 0x49);	//原来0x49
//	wrOV7670Reg(0x5e, 0x0e);	//原来0xe0
//
//	wrOV7670Reg(0x64, 0x04);	//原来0x04	  64-66镜头补偿
//	wrOV7670Reg(0x65, 0x20);	//原来0x20
//	wrOV7670Reg(0x66, 0x05);	//原来0x05
//
//	wrOV7670Reg(0x94, 0x04);	//原来0x04	94-95镜头补偿
//	wrOV7670Reg(0x95, 0x08);	//原来0x08
//
//	wrOV7670Reg(0x6c, 0x0a);	//原来0x0a	6c-6fawb设置
//	wrOV7670Reg(0x6d, 0x55);	//原来0x55
//	wrOV7670Reg(0x6e, 0x11);	//原来0x11
//	wrOV7670Reg(0x6f, 0x9f);   //0x9e for advance AWB	  原来9f
//
//	wrOV7670Reg(0x6a, 0x40);	//原来0x40	g通道awb增益
//	wrOV7670Reg(0x01, 0x40);	//原来0x40	b通道awb增益控制
//	wrOV7670Reg(0x02, 0x40);	//原来0x40	r通道awb增益控制
//
//	//wrOV7670Reg(0x13, 0xe7);	//原来0xe7
//	wrOV7670Reg(0x15, 0x00);   //00  cmos10  含有pclk输出选择
//	wrOV7670Reg(0x4f, 0x80);	//原来0x80	以下为色彩矩阵系数设置
//	wrOV7670Reg(0x50, 0x80);	//原来0x80
//	wrOV7670Reg(0x51, 0x00);	//原来0x00
//	wrOV7670Reg(0x52, 0x22);	//原来0x22
//	wrOV7670Reg(0x53, 0x5e);	//原来0x5e
//	wrOV7670Reg(0x54, 0x80);	//原来0x80
//	wrOV7670Reg(0x58, 0x9e);	//原来0x9e
//
//	wrOV7670Reg(0x41, 0x08);	//原来0x08	com16 以下为边缘设置
//	wrOV7670Reg(0x3f, 0x00);	//原来0x00  边缘增强调整
//	wrOV7670Reg(0x75, 0x05);	//原来0x05
//	wrOV7670Reg(0x76, 0xe1);	//原来0xe1
//
//	wrOV7670Reg(0x4c, 0x00);	//原来0x00  噪声抑制强度
//	wrOV7670Reg(0x77, 0x01);	//原来0x01	噪声去除偏移
//
//	wrOV7670Reg(0x3d, 0xc1);	//0xc0,	   com13 输出设置
//	wrOV7670Reg(0x4b, 0x09);	//原来0x09 寄存器4b设置
//	wrOV7670Reg(0xc9, 0x60);	//原来0x60	 饱和度控制
//	//wrOV7670Reg(0x41, 0x38);	//原来0x38	  com16边缘设置设置
//	wrOV7670Reg(0x56, 0x40);//0x40,  change according to Jim's request	对比度控制
//	wrOV7670Reg(0x34, 0x11);	//原来0x11	  感光阵列参考电压控制
//	wrOV7670Reg(0x3b, 0x02);//0x00,//原来0x02,	  com11控制
//	wrOV7670Reg(0xa4, 0x89);//0x88, 原来89 nt控制
//
//	wrOV7670Reg(0x96, 0x00);	//原来0x00  以下96-9c保留
//	wrOV7670Reg(0x97, 0x30);	//原来0x30
//	wrOV7670Reg(0x98, 0x20);	//原来0x20
//	wrOV7670Reg(0x99, 0x30);	//原来0x30
//	wrOV7670Reg(0x9a, 0x84);	//原来0x84
//	wrOV7670Reg(0x9b, 0x29);	//原来0x29
//	wrOV7670Reg(0x9c, 0x03);	//原来0x03
//	wrOV7670Reg(0x9d, 0x4c);	//原来0x4c	50HZ条纹滤波的值
//	wrOV7670Reg(0x9e, 0x3f);	//原来0x3f	60HZ条纹滤波的值
//
//	wrOV7670Reg(0x09, 0x00);	//原来0x00 通用控制器com2
//	wrOV7670Reg(0x3b, 0xc2);//0x82,//0xc0,//原来0xc2,	//night mode 原来0xc2
//}

//static void init_rgb565_qcif_2fps(void)
//{
//	  wrOV7670Reg(0x11, 0x01); // Pre-scaler clk Ctrl (by 2)
//	  wrOV7670Reg(0x3E, 0x14); // Pclk divider (by 16)
//	  wrOV7670Reg(0x73, 0x00); // Additional Pclk divider bits. (by 2)
//
//	  wrOV7670Reg(0x9d, 0x4c); // 50 Hz band filter value
//	//wrOV7670Reg(0x9e, 0x29); // 60Hz band filter value
//	wrOV7670Reg(0x3b, 0x0A);  // Select 50Hz filter
//	//wrOV7670Reg(0x13, 0xf2);
//	wrOV7670Reg(0x00, 0x00);
//	wrOV7670Reg(0x10, 0x00);
//	wrOV7670Reg(0x01, 0x80);
//	wrOV7670Reg(0x02, 0x80);
//		wrOV7670Reg(0x13, 0xE7); //F7
//	//
////	wrOV7670Reg(0x12, 0x15);
//	wrOV7670Reg(0x0C, 0x08); // Scale enable
//	wrOV7670Reg(0x12, 0x0c); // 0x0c = QCIF + RGB;  0x08 = QCIF + YUV
//	wrOV7670Reg(0x04, 0x00); // CCIR601 disable
//
////	wrOV7670Reg(0x18, 0x4A);
////	wrOV7670Reg(0x17, 0x22);
////	wrOV7670Reg(0x32, 0x89);
////	wrOV7670Reg(0x19, 0x02);
////	wrOV7670Reg(0x1a, 0x7a);
////	wrOV7670Reg(0x03, 0x00);
////	//
////	wrOV7670Reg(0x0e, 0x84);
////	wrOV7670Reg(0x0f, 0x62);
////	wrOV7670Reg(0x15, 0x02);
////	wrOV7670Reg(0x1b, 0x01);
////	wrOV7670Reg(0x1e, 0x01);
////	wrOV7670Reg(0x29, 0x3c);
////	wrOV7670Reg(0x33, 0x00);
////	wrOV7670Reg(0x34, 0x07);
////	wrOV7670Reg(0x35, 0x84);
////	wrOV7670Reg(0x36, 0x00);
////	wrOV7670Reg(0x38, 0x38);  //0x13
////	wrOV7670Reg(0x39, 0x43);
////	wrOV7670Reg(0x3c, 0x68);
//	wrOV7670Reg(0x3d, 0xc0);  //0x19 enable gamma and uv sat autoadj
//		wrOV7670Reg(0x40, 0x10); // modified by me 0xc1   COM15 RGB565
//	wrOV7670Reg(0x69, 0x80);
//	wrOV7670Reg(0x6b, 0x0a); // PLL disable
////	wrOV7670Reg(0xa1, 0x08);
////	//
////	wrOV7670Reg(0x8b, 0xcc);
////	wrOV7670Reg(0x8c, 0xcc);
////	wrOV7670Reg(0x8d, 0xcf);
////	//
//	wrOV7670Reg(0x14, 0x38); //2e  AGC x16
////	wrOV7670Reg(0x25, 0x58);
////	wrOV7670Reg(0x24, 0x68);
//	//wrOV7670Reg(0x71, 0xB5); // TEST PATTERN
//	wrOV7670Reg(0x11, 0x01); // Pre-scaler clk Ctrl (by 2)
//}

static void ICACHE_FLASH_ATTR init_default_regs(void) {
	/* Gamma curve values,  1st to 15th segments */
	wrOV7670Reg( 0x7a, 0x20 );		wrOV7670Reg( 0x7b, 0x10 );
	wrOV7670Reg( 0x7c, 0x1e );		wrOV7670Reg( 0x7d, 0x35 );
	wrOV7670Reg( 0x7e, 0x5a );		wrOV7670Reg( 0x7f, 0x69 );
	wrOV7670Reg( 0x80, 0x76 );		wrOV7670Reg( 0x81, 0x80 );
	wrOV7670Reg( 0x82, 0x88 );		wrOV7670Reg( 0x83, 0x8f );
	wrOV7670Reg( 0x84, 0x96 );		wrOV7670Reg( 0x85, 0xa3 );
	wrOV7670Reg( 0x86, 0xaf );		wrOV7670Reg( 0x87, 0xc4 );
	wrOV7670Reg( 0x88, 0xd7 );		wrOV7670Reg( 0x89, 0xe8 );

	/* AGC and AEC parameters.  Note we start by disabling those features,
	   then turn them only after tweaking the values. */
	wrOV7670Reg( REG_COM8, COM8_FASTAEC | COM8_AECSTEP | COM8_BFILT );
	wrOV7670Reg( REG_GAIN, 0 );
	wrOV7670Reg( REG_AECH, 0 );
	wrOV7670Reg( REG_COM4, 0x40 ); /* magic reserved bit */
	wrOV7670Reg( REG_COM9, 0x18 ); /* 4x gain + magic rsvd bit */
	wrOV7670Reg( REG_BD50MAX, 0x05 );
	wrOV7670Reg( REG_BD60MAX, 0x07 );
	wrOV7670Reg( REG_AEW, 0x75 ); //0x95
	wrOV7670Reg( REG_AEB, 0x63 ); //0x33
	wrOV7670Reg( REG_VPT, 0xe3 );
	wrOV7670Reg( REG_HAECC1, 0x78 );
	wrOV7670Reg( REG_HAECC2, 0x68 );
	wrOV7670Reg( 0xa1, 0x03 ); /* magic */
	wrOV7670Reg( REG_HAECC3, 0xd8 );
	wrOV7670Reg( REG_HAECC4, 0xd8 );
	wrOV7670Reg( REG_HAECC5, 0xf0 );
	wrOV7670Reg( REG_HAECC6, 0x90 );
	wrOV7670Reg( REG_HAECC7, 0x94 );
	wrOV7670Reg( REG_COM8, COM8_FASTAEC|COM8_AECSTEP|COM8_BFILT|COM8_AGC|COM8_AEC );

	/* Almost all of these are magic "reserved" values.  */
	wrOV7670Reg( REG_COM5, 0x61 );	wrOV7670Reg( REG_COM6, 0x4b );
	wrOV7670Reg( 0x16, 0x02 );		wrOV7670Reg( REG_MVFP, 0x37 ); // Flip image vertically
	wrOV7670Reg( 0x21, 0x02 );		wrOV7670Reg( 0x22, 0x91 );
	wrOV7670Reg( 0x29, 0x07 );		wrOV7670Reg( 0x33, 0x0b );
	wrOV7670Reg( 0x35, 0x0b );		wrOV7670Reg( 0x37, 0x1d );
	wrOV7670Reg( 0x38, 0x71 );		wrOV7670Reg( 0x39, 0x2a );
	wrOV7670Reg( REG_COM12, 0x78 );	wrOV7670Reg( 0x4d, 0x40 );
	wrOV7670Reg( 0x4e, 0x20 );		wrOV7670Reg( REG_GFIX, 0 );
//	wrOV7670Reg( 0x6b, 0x4a );
	wrOV7670Reg( 0x74, 0x10 );
	wrOV7670Reg( 0x8d, 0x4f );		wrOV7670Reg( 0x8e, 0 );
	wrOV7670Reg( 0x8f, 0 );		wrOV7670Reg( 0x90, 0 );
	wrOV7670Reg( 0x91, 0 );		wrOV7670Reg( 0x96, 0 );
	wrOV7670Reg( 0x9a, 0 );		wrOV7670Reg( 0xb0, 0x84 );
	wrOV7670Reg( 0xb1, 0x0c );		wrOV7670Reg( 0xb2, 0x0e );
	wrOV7670Reg( 0xb3, 0x82 );		wrOV7670Reg( 0xb8, 0x0a );
	/* More reserved magic, some of which tweaks white balance */
	wrOV7670Reg( 0x43, 0x0a );		wrOV7670Reg( 0x44, 0xf0 );
	wrOV7670Reg( 0x45, 0x34 );		wrOV7670Reg( 0x46, 0x58 );
	wrOV7670Reg( 0x47, 0x28 );		wrOV7670Reg( 0x48, 0x3a );
	wrOV7670Reg( 0x59, 0x88 );		wrOV7670Reg( 0x5a, 0x88 );
	wrOV7670Reg( 0x5b, 0x44 );		wrOV7670Reg( 0x5c, 0x67 );
	wrOV7670Reg( 0x5d, 0x49 );		wrOV7670Reg( 0x5e, 0x0e );
	wrOV7670Reg( 0x6c, 0x0a );		wrOV7670Reg( 0x6d, 0x55 );
	wrOV7670Reg( 0x6e, 0x11 );		wrOV7670Reg( 0x6f, 0x9f ); /* "9e for advance AWB" */
	wrOV7670Reg( 0x6a, 0x40 );		wrOV7670Reg( REG_BLUE, 0x40 );
	wrOV7670Reg( REG_RED, 0x60 );
	wrOV7670Reg( REG_COM8, COM8_FASTAEC|COM8_AECSTEP|COM8_BFILT|COM8_AGC|COM8_AEC|COM8_AWB );
	/* Matrix coefficients */
	wrOV7670Reg( 0x4f, 0x80 );		wrOV7670Reg( 0x50, 0x80 );
	wrOV7670Reg( 0x51, 0 );		wrOV7670Reg( 0x52, 0x22 );
	wrOV7670Reg( 0x53, 0x5e );		wrOV7670Reg( 0x54, 0x80 );
	wrOV7670Reg( 0x58, 0x9e );
	wrOV7670Reg( REG_COM16, COM16_AWBGAIN );	wrOV7670Reg( REG_EDGE, 0 );
	wrOV7670Reg( 0x75, 0x05 );		wrOV7670Reg( 0x76, 0xe1 );
	wrOV7670Reg( 0x4c, 0 );		wrOV7670Reg( 0x77, 0x01 );
	wrOV7670Reg( REG_COM13, 0xc3 );	wrOV7670Reg( 0x4b, 0x09 );
	wrOV7670Reg( 0xc9, 0x60 );		wrOV7670Reg( REG_COM16, 0x38 );
	wrOV7670Reg( 0x56, 0x40 );
	wrOV7670Reg( 0x34, 0x11 );		wrOV7670Reg( REG_COM11, COM11_EXP|COM11_HZAUTO );
	wrOV7670Reg( 0xa4, 0x88 );		wrOV7670Reg( 0x96, 0 );
	wrOV7670Reg( 0x97, 0x30 );		wrOV7670Reg( 0x98, 0x20 );
	wrOV7670Reg( 0x99, 0x30 );		wrOV7670Reg( 0x9a, 0x84 );
	wrOV7670Reg( 0x9b, 0x29 );		wrOV7670Reg( 0x9c, 0x03 );
	wrOV7670Reg( 0x9d, 0x4c );		wrOV7670Reg( 0x9e, 0x3f );
	wrOV7670Reg( 0x78, 0x04 );
	/* Extra-weird stuff.  Some sort of multiplexor register */
	wrOV7670Reg( 0x79, 0x01 );		wrOV7670Reg( 0xc8, 0xf0 );
	wrOV7670Reg( 0x79, 0x0f );		wrOV7670Reg( 0xc8, 0x00 );
	wrOV7670Reg( 0x79, 0x10 );		wrOV7670Reg( 0xc8, 0x7e );
	wrOV7670Reg( 0x79, 0x0a );		wrOV7670Reg( 0xc8, 0x80 );
	wrOV7670Reg( 0x79, 0x0b );		wrOV7670Reg( 0xc8, 0x01 );
	wrOV7670Reg( 0x79, 0x0c );		wrOV7670Reg( 0xc8, 0x0f );
	wrOV7670Reg( 0x79, 0x0d );		wrOV7670Reg( 0xc8, 0x20 );
	wrOV7670Reg( 0x79, 0x09 );		wrOV7670Reg( 0xc8, 0x80 );
	wrOV7670Reg( 0x79, 0x02 );		wrOV7670Reg( 0xc8, 0xc0 );
	wrOV7670Reg( 0x79, 0x03 );		wrOV7670Reg( 0xc8, 0x40 );
	wrOV7670Reg( 0x79, 0x05 );		wrOV7670Reg( 0xc8, 0x30 );
	wrOV7670Reg( 0x79, 0x26 );

}

static void ICACHE_FLASH_ATTR init_format_rgb565(void) {
	wrOV7670Reg( REG_COM7, COM7_RGB );	/* Selects RGB mode */
	wrOV7670Reg( RGB444RegId, 0 );	/* No RGB444 please */
	wrOV7670Reg( REG_COM1, 0x0 );	/* CCIR601 */
	wrOV7670Reg( REG_COM15, 0x10 );
	wrOV7670Reg( REG_COM9, 0x38 ); 	/* 16x gain ceiling; 0x8 is reserved bit */
	wrOV7670Reg( 0x4f, 0xb3 );	/* "matrix coefficient 1" */
	wrOV7670Reg( 0x50, 0xb3 ); 	/* "matrix coefficient 2" */
	wrOV7670Reg( 0x51, 0    );		/* vb */
	wrOV7670Reg( 0x52, 0x3d ); 	/* "matrix coefficient 4" */
	wrOV7670Reg( 0x53, 0xa7 ); 	/* "matrix coefficient 5" */
	wrOV7670Reg( 0x54, 0xe4 ); 	/* "matrix coefficient 6" */
	wrOV7670Reg( REG_COM13, 0x80|0x40 );
	}

static void ICACHE_FLASH_ATTR init_rgb565_qcif(void) {
	wrOV7670Reg(REG_CLKRC, CLK_SCALE);  // XClk Pre-scaler (div by (CLK_SCALE + 1))
	wrOV7670Reg(REG_COM3, (0x08|0x04) ); // Enable scaling and DCW
	wrOV7670Reg(REG_COM3, 0x04 );
	wrOV7670Reg(REG_COM14, (0x10 | 0x01) ); // Pclk (div by 2)
	wrOV7670Reg(0x73, 0xf1 );   // Further div Pclk (by 2) TOTAL: 2*2*16=64
	wrOV7670Reg(0xa2, 0x52 );   // magic
	wrOV7670Reg(0x7b, 0x1c );   // gamma 2
	wrOV7670Reg(0x7c, 0x28 );   // gamma 3
	wrOV7670Reg(0x7d, 0x3c );   // gamma 4
	wrOV7670Reg(0x7f, 0x69 );   // gamma 6
	wrOV7670Reg(REG_COM9, 0x38 );  // 0x38 Max AGC gain x16
	wrOV7670Reg(0xa1, 0x0b );   // magic
	wrOV7670Reg(0x74, 0x19 );   // Digital gain to manual
	wrOV7670Reg(0x9a, 0x80 );   // magic
	wrOV7670Reg(0x43, 0x14 );   // magic
	wrOV7670Reg(REG_COM13, 0xc0 ); // Enable Gamma & UV Sat to auto
	wrOV7670Reg(REG_CLKRC, CLK_SCALE);  // XClk Pre-scaler (div by (CLK_SCALE + 1)) again (why?)

	//wrOV7670Reg(Com7RegId, 0x00);
	//wrOV7670Reg(0x71, 0xB5); // TEST PATTERN
}


/******************************************************************************
 * FunctionName : user_init
 * Description  : init function
 * Parameters   : none
 * Returns      : none
 *******************************************************************************/
bool ICACHE_FLASH_ATTR
camera_init(void)
{
	i2c_master_gpio_init();

	uint8 temp=0x80;
	if(0==wrOV7670Reg(REG_COM7, temp)) //Reset SCCB via com7
	{
        return false;
	}
	os_delay_us(100000);

	init_default_regs();
	os_delay_us(10000);
	init_format_rgb565();
	os_delay_us(10000);
	init_rgb565_qcif();

	os_delay_us(10000);
	ov7670_set_hw(456, 24, 14, 494);



		ets_uart_printf("\n");
		ets_uart_printf("OV7670 reg cfg done!\n");
		user_gpio_init();

	return true;
}


