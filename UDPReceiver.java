+package udpreceiver;
+
+import java.awt.image.BufferedImage;
+import java.awt.image.WritableRaster;
+import java.io.ByteArrayOutputStream;
+import java.io.File;
+import java.io.FileOutputStream;
+import java.net.DatagramPacket;
+import java.net.DatagramSocket;
+import java.text.Format;
+import java.text.SimpleDateFormat;
+import java.util.Arrays;
+import java.util.Date;
+import javax.imageio.ImageIO;
+
+public class UDPReceiver {
+
+    private static final int HRESOL = 176;
+    private static final int VRESOL = 144;
+    private static final int PACKET_SIZE = 1;
+    
+    public static void main(String args[]) {
+        try {
+            int port = 8100;
+
+            // Create a socket to listen on the port.
+            DatagramSocket dsocket = new DatagramSocket(port);
+
+            // Create a buffer to read datagrams into. If a
+            // packet is larger than this buffer, the
+            // excess will simply be discarded!
+            byte[] buffer = new byte[PACKET_SIZE*HRESOL*2+1];
+            byte[] imgData = new byte[2*HRESOL*VRESOL];
+            int sof = 0;
+
+            // Create a packet to receive data into the buffer
+            DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
+            
+            // Now loop forever, waiting to receive packets and printing them.
+            while (true) {
+                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
+
+                // Receive lines and append them to form a frame
+                for (int i = 0; i < VRESOL/PACKET_SIZE; i++) {
+                    // Wait to receive a datagram (line)
+                    dsocket.receive(packet);
+                    // Syncro to start of frame and append to image byte[]            
+                    if (packet.getLength() == PACKET_SIZE*HRESOL*2) {
+                        outputStream.write(Arrays.copyOfRange(buffer, 1, buffer.length));
+                        Date date = new Date();
+                        System.out.println("Detected SOF! time: " + System.currentTimeMillis() + "\n");
+                        sof = 1;
+                    } else if (sof == 1) {
+                        outputStream.write(Arrays.copyOfRange(buffer, 0, buffer.length - 1));
+                    } else {
+                        break;
+                    }
+                }
+
+                // Go on only if Start of Frame was detected.
+                if (sof == 1) {
+
+                    imgData = outputStream.toByteArray();
+                    
+                    // Send raw contents to file
+                    	File file = new File("YOURPATH\\image_raw");
+			FileOutputStream fop = new FileOutputStream(file);
+			// if file doesnt exists, then create it
+			if (!file.exists()) {
+				file.createNewFile();
+			}
+			fop.write(imgData);
+			fop.flush();
+			fop.close();
+                    
+                    // Reset the length of the packet before reusing it.
+                    packet.setLength(buffer.length);
+
+                    // Form image and write to disk
+                    //construct image, row-major order
+                    BufferedImage img = new BufferedImage(HRESOL, VRESOL, BufferedImage.TYPE_USHORT_565_RGB);
+                    WritableRaster rast = img.getRaster();
+                    short[] rgb16 = new short[1];
+                    int i = 0;
+                    for (int y = 0; y < VRESOL; y++) {
+                        for (int x = 0; x < HRESOL; x++) {
+                            rgb16[0] = (short) ((imgData[i++] << 8) + imgData[i++]);
+                            rgb16[0] = (short) (((rgb16[0] & 0xFF00) >> 8) | ((rgb16[0] & 0x00FF) << 8)); // Change endianness to little.
+                            rast.setDataElements(x, y, rgb16);
+                        }
+                    }
+                    
+                    //Date date = new Date();
+                    //System.out.println(Arrays.toString(imgData));                    
+                    //Format formatter = new SimpleDateFormat("YYYY-MM-dd_hh-mm-ss");
+                    //ImageIO.write(img, "jpg", new File("YOURPATH\\imagen_udp" + formatter.format(date) + ".jpg"));
+                    ImageIO.write(img, "png", new File("YOURPATH\\imagen_udp.png"));
+                    sof = 0;
+                }
+            }
+        } catch (Exception e) {
+            System.err.println(e);
+        }
+    }
+}