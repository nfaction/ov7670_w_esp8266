#ifndef __USER_GPIO_H__
#define __USER_GPIO_H__

#define H_RESOL 		352  //twice the horizontal resolution (16 bits per pixel)
#define V_RESOL 		144
#define PACKET_SIZE		1

#define TESTING 		0
#define DEBUG 			0

extern char line[H_RESOL];
extern int DevBuffer_length;
void user_gpio_init(void);

#endif
