#ifndef __UDP_CLIENT_H__
#define __UDP_CLIENT_H__


void ICACHE_FLASH_ATTR user_udp_send(void);
void ICACHE_FLASH_ATTR user_set_station_config(void);
void ICACHE_FLASH_ATTR user_udp_recv_cb(void *arg, char *pusrdata, unsigned short length);
void ICACHE_FLASH_ATTR user_udp_sent_cb(void *arg);
void ICACHE_FLASH_ATTR user_check_ip(void);
void ICACHE_FLASH_ATTR user_set_station_config(void);
void udp_init(void);

#endif
