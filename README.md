This small project represents a test of an OV7670 camera connected to ESP8266. It retrieves pictures in QCIF format at a rate of aprox 1 frame per 2.5 seconds, when using an XCLK of 11MHz, and sends them through UDP to a given IP address and port.

PCLK is divided by 64 so that ESP8266 module can withstand the data rate (50KB per picture).

Parts of this project were taken from Esspresif's bbs examples. Others, like registers configs for the camera module, were taken from the linux driver for OV7670.

Due to constraints in the number of GPIOs available in ESP8266 module, some of the pins have to be shared, and so a switch should handle the toggling of these after boot up (GPIO0, GPIO1, GPIO2, GPIO15).

A Java server to save images to disk is as well provided for testing purposes.


Enjoy.


![ov7670_w_esp8266.png](https://bitbucket.org/repo/naEnAG/images/3638156771-ov7670_w_esp8266.png)